CC = clang -g -Wall -m32
AR = llvm-ar

all: bin bin/asm bin/emu

bin:
	mkdir bin

clean:
	rm -f bin/*

bin/nvm.a: nvm.c nvm.h
	${CC} -c $< -o bin/nvm.o
	${AR} -rv $@ bin/nvm.o

bin/asm: asm.c bin/nvm.a
	${CC} $^ -o $@

bin/emu: emu.c bin/nvm.a
	${CC} $^ -o $@

bin/test.nvm: test.nal bin/asm
	valgrind ./bin/asm $< $@
test: bin/emu bin/test.nvm
	valgrind $< bin/test.nvm

bin/throf.nvm: throf.nal bin/asm
	./bin/asm $< $@
throf: bin/emu bin/throf.nvm
	$< bin/throf.nvm
